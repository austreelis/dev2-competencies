# DEV-202

## Competence description

The student is able to write small concise and fast python scripts to interact
with information systems and automate administrative tasks.

These scripts use system libraries to manipulate the file system, interact with
processes, network communications. They are not interactive, but have a
well-defined command line interface.

The student respect python programming good practices: good variable naming,
PEP8 compliance, good usage of control flow, good readability, etc.

## Implementation description

This module is a simple re-implementation of vim's [`xxd`][xxd-man] utility.

`xxd` is a binary file dump generator. For instance, when given as input a file
with following content:

```markdown
# Funny chimpanzee

The monkeys go banana when given some nuts.
```

Generates, with default arguments, the following dump:

```
00000000: 2320 4675 6e6e 7920 6368 696d 7061 6e7a  # Funny chimpanz
00000010: 6565 0a0a 5468 6520 6d6f 6e6b 6579 7320  ee..The monkeys
00000020: 676f 2062 616e 616e 6120 7768 656e 2067  go banana when g
00000030: 6976 656e 2073 6f6d 6520 6e75 7473 2e0a  iven some nuts..
```

Note the output consists of three main columns: The offset in the file, the
data itself in groups of bytes, and the ascii representation of the data if
printable, replaced with a dot otherwise.

`xxd` is a powerful tool, able to output in c-style include or postscript
plain hexdump format, or to recreate a file from one of its dumps. For this
exercise, we will focus on outputting binary hexdumps in the default format
only, as shown above. However, to bring a little more fun, we will add a feature
not present in `xxd`: uploading dumps to [pastebin](https://pastebin.com/).

The module consists of a script that is:

- Able to parse arguments from the command line.
- Print meaningful errors on standard error.
- Generate a dump whose format can be altered by arguments.
- Output the dump to a file or standard output.
- Upload the dump to pastebin (using the `requests` package).

## Validated competencies

As per the student's appreciation, this module demonstrates they are able to:

- [x] Write small concise and fast python scripts.
- [x] Write scripts interacting with information systems and automating 
  administrative tasks.
- [x] Use system libraries to:
  - [x] Manipulate the file system.
  - [ ] Interact with processes.
  - [x] Interact with network communications.
- [x] Write a well-defined command-line interface.
- [x] Respect programming good practices.
  - [x] Correctly name variables.
  - [x] Write PEP8-compliant code.
  - [x] Correctly use control flow statements.
  - [x] Write readable code.

## Command-line interface

```
Usage:
        dev-202 [options] [infile [outfile]]
Options:
    -c cols        format <cols> octets per line. Default 16.
    -e             little-endian dump.
    -g group_size  number of octets per group in normal output. Default 2.
    -h --help      print this summary.
    -l len         stop after <len> octets.
    -o off         add <off> to the displayed file position.
    -p dev_key     upload dump to pastebin and output url. Disables dumping to
                   stdout. To obtain your dev key, create a pastebin account
                   and go to the "API" tab.
    -d             show offset in decimal instead of hex.
    -s [-]seek  start at <seek> bytes abs. (or -: from end) infile offset.
    -u             use upper case hex letters.
    -v --version   show version.
```

## Usage example

These commands are run from the repository's root.

### Base case without options

`python -m dev202 dev202/testfile`

<details>
<summary>Output</summary>

Python:

```
00000000: 456d 6f6a 6973 2061 7265 2061 2067 7265  Emojis are a gre
00000010: 6174 2077 6179 2074 6f20 636f 6d6d 756e  at way to commun
00000020: 6963 6174 6520 656d 6f74 696f 6e73 20f0  icate emotions .
00000030: 9f98 84f0 9f91 8c0a 4275 7420 7768 6174  ........But what
00000040: 2049 206c 6f76 6520 f09f 988d 2074 6865   I love .... the
00000050: 206d 6f73 7420 6162 6f75 7420 7468 656d   most about them
00000060: 2069 7320 6d61 6b69 6e67 20f0 9fa9 bbf0   is making .....
00000070: 9f9a 8c0a                                ....
```

`xxd dev202/testfile`:

```
00000000: 456d 6f6a 6973 2061 7265 2061 2067 7265  Emojis are a gre
00000010: 6174 2077 6179 2074 6f20 636f 6d6d 756e  at way to commun
00000020: 6963 6174 6520 656d 6f74 696f 6e73 20f0  icate emotions .
00000030: 9f98 84f0 9f91 8c0a 4275 7420 7768 6174  ........But what
00000040: 2049 206c 6f76 6520 f09f 988d 2074 6865   I love .... the
00000050: 206d 6f73 7420 6162 6f75 7420 7468 656d   most about them
00000060: 2069 7320 6d61 6b69 6e67 20f0 9fa9 bbf0   is making .....
00000070: 9f9a 8c0a                                ....
```

Diff: None

</details>

`python -m dev202 -c 15 -g 5 dev202/testfile`

<details>
<summary>Output</summary>

Python:
```
00000000: 456d6f6a69 7320617265 2061206772  Emojis are a gr
0000000f: 6561742077 617920746f 20636f6d6d  eat way to comm
0000001e: 756e696361 746520656d 6f74696f6e  unicate emotion
0000002d: 7320f09f98 84f09f918c 0a42757420  s .........But
0000003c: 7768617420 49206c6f76 6520f09f98  what I love ...
0000004b: 8d20746865 206d6f7374 2061626f75  . the most abou
0000005a: 7420746865 6d20697320 6d616b696e  t them is makin
00000069: 6720f09fa9 bbf09f9a8c 0a          g .........
```

`xxd -c 15 -g 5 dev202/testfile`:

```
00000000: 456d6f6a69 7320617265 2061206772  Emojis are a gr
0000000f: 6561742077 617920746f 20636f6d6d  eat way to comm
0000001e: 756e696361 746520656d 6f74696f6e  unicate emotion
0000002d: 7320f09f98 84f09f918c 0a42757420  s .........But
0000003c: 7768617420 49206c6f76 6520f09f98  what I love ...
0000004b: 8d20746865 206d6f7374 2061626f75  . the most abou
0000005a: 7420746865 6d20697320 6d616b696e  t them is makin
00000069: 6720f09fa9 bbf09f9a8c 0a          g .........
```

Diff: None

</details>

`python -m dev202 -s -31 -l 25 -g 1 -c 5 -o 4 dev202/testfile`

<details>
<summary>Output</summary>

Python:
```
00000059: 20 61 62 6f 75   abou
0000005e: 74 20 74 68 65  t the
00000063: 6d 20 69 73 20  m is
00000068: 6d 61 6b 69 6e  makin
0000006d: 67 20 f0 9f a9  g ...
```

`xxd -s -31 -l 25 -g 1 -c 5 -o 4 dev202/testfile`:

```
00000059: 20 61 62 6f 75   abou
0000005e: 74 20 74 68 65  t the
00000063: 6d 20 69 73 20  m is
00000068: 6d 61 6b 69 6e  makin
0000006d: 67 20 f0 9f a9  g ...
```

Diff: None

</details>

`python -m dev202 -p $PASTEBIN_DEV_KEY dev202/testfile`

Output: [`https://pastebin.com/YHZz6zHX`](https://pastebin.com/YHZz6zHX)

`python -m dev202 dev202/testfile dump`

Output: nothing, but the file `dump` contains the dump output.

`python -m dev202 -v`

Output: prints module version.

`python -m dev202 -h`

Output: prints module description, copyright, version and usage.

Following are some invalid use examples:

`python -m dev202`

Output: prints usage.

`python -m dev202 a b c`

Output: prints usage.

`python -m dev202 -x a`

Output: prints usage.

`python -m dev202 /non/existent/path`

Output: `FileNotFoundError: [Errno 2] No such file or directory: '/non/existent/path'`.

`python -m dev202 -c f testfile`

Output:
```
Invalid column format 'f'
Expected a base 10 integer
```

`python -m dev202 -c -3 testfile`

Output:
```
Invalid column format '-3'
Must be greater than 0
```

`python -m dev202 -c 15 -g 4 testfile`

Output: `Column (15) must be an integer multiple of group size (4)`

`python -m dev202 -s f testfile`

Output:
```
Invalid seek format 'f'
Expected a base 10 integer
```

[xxd-man]: https://github.com/vim/vim/blob/master/runtime/doc/xxd.man
